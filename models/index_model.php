<?php

class Index_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function xhrGetListings()
    {
        $sth = $this->db->prepare('SELECT * FROM content WHERE frontpage = "yes" ORDER BY date DESC');
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();
        $data = $sth->fetchAll();
        echo json_encode($data);
    }

}