<?php

class Content_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function article($id)
    {
        $sth = $this->db->prepare('SELECT id, author, date, title, message FROM content WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
        return $sth->fetch();
    }

    public function contentList()
    {
        $sth = $this->db->prepare('SELECT * FROM content');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function createarticle($data){
        $sth = $this->db->prepare('INSERT INTO content (`type`, `title`, `slug`, `category`, `author`, `date`, `message`, `frontpage`) VALUES (:type, :title, :slug, :category, :author, now(), :message, :frontpage)');
        $sth->execute(array(
            ':type' => 'article',
            ':title' => $data['title'],
						':slug' =>$data['slug'],
						':category' =>$data['category'],
            ':author' => $data['author'],
            //':date' => $data['date'],
            ':message' => $data['message'],
            ':frontpage' => $data['frontpage']
        ));
if (!$sth->execute()) {
    print_r($sth->errorInfo());
}
    }
    
    public function delete($id){
        $sth = $this->db->prepare('DELETE FROM content WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
    }

}
