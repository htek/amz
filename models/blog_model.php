<?php

class Blog_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }
    public function blogList()
    {
        $sth = $this->db->prepare('SELECT * FROM content');
        $sth->execute();
        return $sth->fetchAll();
    }
    public function createblog($data){
        $sth = $this->db->prepare('INSERT INTO content (`type`, `title`, `slug`, `category`, `author`, `date`, `message`) VALUES (:type, :title, :slug, :category, :author, now(), :message)');
        $sth->execute(array(
						':type' => $data['type'],
            ':title' => $data['title'],
						':slug' => $data['slug'],
						':category' => $data['category'],
            ':author' => $data['author'],
            //':date' => $data['date'],
            ':message' => $data['message']
        ));
				echo "success";
    }
    public function delete($id){
        $sth = $this->db->prepare('DELETE FROM content WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
    }

}
