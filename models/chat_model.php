<?php

class Chat_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function ajaxLoad()
    {
        $sth = $this->db->prepare("SELECT * FROM chat WHERE chattime >= CURDATE() ORDER BY chattime");
        $sth->execute();
        while($result = $sth->fetch(PDO::FETCH_ASSOC)) {
            $time = strtotime($result['chattime']);
            //$curTime = time();
                echo '<div id="chat">'.'<span id="nickname">'.$result['nickname'].'</span>'.'<span id="chatdate">'.date('@ g:ia', $time).'</span>'.": " . $result['chattext'].'<br>'.'</div>';

        }
    }

    public function ajaxSubmit($data){
        $sth = $this->db->prepare('INSERT INTO chat (`nickname`, `chattext`) VALUES (:nickname, :chattext)');
        $sth->execute(array(
            ':nickname' => $data['nickname'],
            ':chattext' => $data['chattext']
        ));

    }
    
}