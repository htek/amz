<?php

class Portfolio_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }



    public function article($id)
    {
        $sth = $this->db->prepare('SELECT id, author, date, title, message FROM content WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
        return $sth->fetch();
    }

    public function portfolioList()
    {
        $sth = $this->db->prepare('SELECT * FROM portfolio');
        $sth->execute();
        return $sth->fetchAll();
    }

    public function createPortfolio($data){
        $sth = $this->db->prepare('INSERT INTO portfolio (`type`, `title`, `author`, `date`, `message`) VALUES (:type, :title, :author, :date, :message)');
        $sth->execute(array(
            ':type' => 'blog',
            ':title' => $data['title'],
            ':author' => $data['author'],
            ':date' => $data['date'],
            ':message' => $data['message']
        ));
    }


    public function delete($id){
        $sth = $this->db->prepare('DELETE FROM portfolio WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
    }

}