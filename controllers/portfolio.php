<?php

class Portfolio extends Controller {

    function __construct()
    {
        parent::__construct();
        /*Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false){
            Session::destroy();
            header('location: ../login');
            exit;
        }
        */

    }
    function index()
    {
        $this->view->render('portfolio/index');
    }
    function logout()
    {
        Session::destroy();
        header('location: ../login');
        exit;
    }

    public function createportfolio(){
        $data = array();
        $data['type'] = "portfolio";
        $data['title'] = $_POST['title'];
        $data['author'] = $_POST['author'];
        $data['date'] = $_POST['date'];
        $data['message'] = $_POST['message'];

        //Do error checking
        $this->model->createportfolio($data);

        header('Location: ../portfolio');
    }


    public function contentFormPortfolio(){
        $this->view->render('portfolio/createportfolio');

    }
    public function viewPortfolio(){
        $this->view->portfolioList = $this->model->portfolioList();
        $this->view->render('portfolio/viewportfolio');
    }


    public function edit($id){

        //fetch individual user
        $this->view->user = $this->model->portfolioList($id);
        $this->view->render('portfolio/edit');
    }
    public function delete($id){
        $this->model->delete($id);
        header('location: ' . URL . 'portfolio/viewportfolio');
    }


}