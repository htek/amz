<?php

class Content extends Controller {

    function __construct()
    {
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false){
            Session::destroy();
            header('location: ../login');
            exit;
        }

    }
    function index()
    {
        $this->view->render('content/index');
    }
    function logout()
    {
        Session::destroy();
        header('location: ../login');
        exit;
    }
    public function createarticle(){
        $text_message = $_POST['message'];
        $text_nl2br = nl2br($text_message);
        $text_strip = strip_tags($text_nl2br, '<br>');

        $data = array();
        $data['type'] = "article";
        $data['title'] = $_POST['title'];
				$data['slug'] = $_POST['slug'];
				$data['category'] = $_POST['category'];
        $data['author'] = $_POST['author'];
        $data['date'] = $_POST['date'];
        $data['message'] = $text_strip;

        if (isset($_POST['frontpage'])) {
            $data['frontpage'] = "yes";
        } else {
            $data['frontpage'] = "no";

        }

        //Do error checking
        $this->model->createarticle($data);

        header('Location: ../content/viewcontent');
    }
    public function createportfolio(){
        $data = array();
        $data['type'] = "portfolio";
        $data['title'] = $_POST['title'];
        $data['author'] = $_POST['author'];
        $data['date'] = $_POST['date'];
        $data['message'] = $_POST['message'];

        //Do error checking
        $this->model->createportfolio($data);

        header('Location: ../content');
    }


    public function article($id)
    {
        $this->view->test = $this->model->article($id);
        $this->view->render('content/articletemplate');
    }

    public function contentFormPortfolio(){
        $this->view->render('content/createportfolio');

    }
    
    public function contentForm(){
        $this->view->render('content/createarticle');
    }
    public function viewContent(){
        $this->view->contentList = $this->model->contentList();
        $this->view->render('content/viewcontent');
    }


    public function edit($id){

        //fetch individual user
        $this->view->user = $this->model->contentList($id);
        $this->view->render('content/edit');
    }
    public function delete($id){
        $this->model->delete($id);
        header('location: ' . URL . 'content/viewcontent');
    }


}
