<?php

class Users extends Controller {

    function __construct()
    {
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false){
            Session::destroy();
            header('location: ../login');
            exit;
        }
        $this->view->js = array('users/js/default.js');

    }
 /*-- Shows the list of users on the user index -- */
    function index()
    {
        $this->view->userList = $this->model->userList();
        $this->view->render('users/index');
    }

/*-- Creates User with username, password, and a role -- */
    public function create()
    {
        $data = array();
        $data['username'] = $_POST['username'];
        $data['password'] = $_POST['password'];
        $data['role'] = $_POST['role'];

        //Do your error checking
        $this->model->create($data);

        header('location: ' . URL . 'users');
    }

/*-- Edit User information, username, password -- */
    public function edit($id){

        //fetch individual user
        $this->view->user = $this->model->userSingleList($id);
        $this->view->render('users/edit');

    }
/*-- Delete User -- */
    public function delete($id){
        $this->model->delete($id);
        header('location: ' . URL . 'users');
    }
     function logout()
    {
        Session::destroy();
        header('location: ../login');
        exit;
    }
}
