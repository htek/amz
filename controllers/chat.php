<?php

class Chat extends Controller {

    function __construct()
    {
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false){
            Session::destroy();
            header('location: ../login');
            exit;
        }
        $this->view->js = array('chat/js/main.js');

    }
    function index()
    {
        $this->view->render('chat/index');
    }
    function logout()
    {
        Session::destroy();
        header('location: ../login');
        exit;
    }
    function ajaxLoad(){
        $this->model->ajaxLoad();

    }
    function ajaxSubmit()
    {
        $data = array();
        $data['nickname'] = $_SESSION['nickname'];
        $data['chattext'] = $_POST['chattext'];

        $this->model->ajaxSubmit($data);

        header('Location: ../chat');

    }
}