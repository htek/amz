<?php

class Blog extends Controller {

    function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $this->view->render('blog/index');
    }
    function logout()
    {
        Session::destroy();
        header('location: ../login');
        exit;
    }
    public function createblog(){
				$text_message = $_POST['message'];
				$text_nl2br = nl2br($text_message);
				$text_strip = strip_tags($text_nl2br, '<br>');

        $data = array();
				$data['type'] = "blog";
        $data['title'] = $_POST['title'];
				$data['slug'] = $_POST['slug'];
				$data['category'] = $_POST['category'];
        $data['author'] = $_POST['author'];
        $data['date'] = $_POST['date'];
        $data['message'] = $text_strip; 
        //Do error checking
        $this->model->createblog($data);

        //header('Location: ../blog');
    }
    public function contentFormBlog(){
        $this->view->render('blog/createblog');
    }
    public function viewBlog(){
        $this->view->blogList = $this->model->blogList();
        $this->view->render('blog/viewblog');
    }
    public function edit($id){
        //fetch individual user
        $this->view->user = $this->model->blogList($id);
        $this->view->render('blog/edit');
    }
    public function delete($id){
        $this->model->delete($id);
        header('location: ' . URL . 'blog/viewblog');
    }
}
