
<div class="row">
    <?php
    $link = $_SERVER['REQUEST_URI'];
    $replace = str_replace("/", "", $link);
    ?>
</div>
<div class="row">
    <div class="col-md-12"><h3 id="content-title">Blog List</h3></div>
</div>
<div class="row">

    <div class="col-md-12">
        <div class="content-view">
            <table id="contentlist">
                <th>ID</th>
                <th>Type</th>
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
                <?php
                foreach($this->blogList as $key => $value) {
                    echo '<tr>';
                    echo '<td>'.$value['id'].'</td>';
                    echo '<td>'.$value['title'].'</td>';
                    echo '<td>'.$value['author'].'</td>';
                    echo '<td>'.$value['date'].'</td>';
                    //echo '<td>'.$value['message'].'</td>';
                    echo '<td><a href="'.URL.'blog/article/'.$value['id'].'">View</a>&nbsp;';
                    echo '<td><a href="'.URL.'blog/edit/'.$value['id'].'">Edit</a>&nbsp;';
                    echo '<td><a href="'.URL.'blog/delete/'.$value['id'].'">Delete</a></td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>
    </div>
</div>

</div>
