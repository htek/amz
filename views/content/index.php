<?php
$link = $_SERVER['REQUEST_URI'];
$replace = str_replace("/", "", $link);

?>
<div class="row">
    <div class="col-md-12 pagetitle"><h2>Content</h2></div>

</div>

<div class="row content-panel">
    <div class="col-md-3"></div>
    <div class="col-md-2">
        <div class="content-button">
            <a href="<?php echo URL ?>content/contentForm">Create Article</a>
        </div>
    </div>
    <div class="col-md-2">
        <div class="content-button">
            <a href="<?php echo URL ?>blog/contentFormBlog">Create Blog</a>
        </div>
    </div>
    <div class="col-md-2">
        <div class="content-button">
            <a href="
            <?php
                if($_SERVER['REQUEST_URI'] !== URL."content")
                {
                    echo URL."content/viewContent";
                } else {
                    echo URL."/viewContent";
                }
            ?>
            ">View Content</a>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
<div class="row content-panel">
    <div class="col-md-3"></div>
    <div class="col-md-2">
        <div class="content-button">
            <a href="<?php URL ?>portfolio/contentFormPortfolio">Create Portfolio</a>
        </div>
    </div>
</div>
