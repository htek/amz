<nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
  <a class="navbar-brand" href="#">Christopher Hardee</a>
  <ul class="nav navbar-nav">
    <li class="nav-item active">
			<a class="nav-link" href="<?php echo URL; ?>">Home<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
			<a class="nav-link" href="<?php echo URL; ?>portfolio">Portfolio</a>
    </li>
    <li class="nav-item">
			<a class="nav-link" href="<?php echo URL; ?>blog">Blog</a>
		</li>
			<?php
			if ((Session::get('loggedIn') == true) && (Session::get('role') == 'admin') or (Session::get('role') == 'Owner')):?>
		<li class="nav-item">
			<a class="nav-link" href="<?php echo URL; ?>dashboard">Admin</a>
		</li>
			<?php endif; ?>	
			<?php 
			if(Session::get('loggedIn') == true): ?>
		<li class="nav-item">
        <a class="nav-link" href="<?php echo URL; ?>account">View Profile</a>
		</li>
			<?php endif; ?>
  </ul>
	<form class="form-inline float-xs-right">
		<?php
		if(Session::get('loggedIn') == true): ?>
    <button class="btn btn-success" type="button"><a class="nav-link text-white" href="<?php echo URL; ?>dashboard/logout">Logout</a></button>
		<?php else: ?>
		 <button class="btn btn-success bg-info" type="button"><a class="nav-link text-white" href="<?php echo URL; ?>login">login</a></button>
		<?php endif; ?>
  </form>
</nav>
