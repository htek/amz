<div class="row">
<div class="col-md-2"></div>
<div class="col-md-10">
<h1>Welcome to Puppybot</h1>
<h3>Spam Puppies!</h3>
</div>
</div>

<div class="row">
<div class="col-md-8">
<form class="form-horizontal" action="" method="post">
  <div class="form-group">
    <label for="inputPhone3" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
      <input type="phone" class="form-control" id="inputPhone3" placeholder="phone">
    </div>
  </div>
  <div class="form-group">
	<label for="inputCarrier3" class="col-sm-2 control-label">Carrier</label>
	<div class="col-sm-10">
	<input type="carrier" class="form-control" id="inputCarrier3" placeholder="carrier">
	</div>
   </div>
  <div class="form-group">
    <label for="inputFrom3" class="col-sm-2 control-label">From</label>
    <div class="col-sm-10">
      <input type="from" class="form-control" id="inputFrom3" placeholder="from">
    </div>
  </div>
  <div class="form-group">
    <label for="inputSubject3" class="col-sm-2 control-label">Subject</label>
    <div class="col-sm-10">
      <input type="subject" class="form-control" id="inputSubject3" placeholder="Subject">
    </div>
  </div>
<div class="form-group">
   <label for="inputPicture3" class="col-sm-2 control-label">Picture</label>
   <div class="col-sm-10">
	<input type="file" class="form-control" id="inputPicture3" placeholder="picture">
   </div>
</div>
<div class="form-group">
    <label for="inputMessage3" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
	<textarea class="form-control" rows="3"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Send</button>
    </div>
  </div>
</form>
</div>
</div>
