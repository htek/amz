$( document ).ready(function() {

    $(function () {
        var myArray = $('.slideritem');
        var arrayIndex = 0;

        function test(){
            x = 5;
            console.log(x);

        }

        setInterval(test(), 3000);
        //Moves newsitems up on y axis
        $('#down').click(function () {
            console.log("test");

            $(myArray[arrayIndex++]).slideUp()

            //disables button when at array length. -1 to matchup the 						//click counter to the array length
            if (arrayIndex == myArray.length - 1)
                $('#down').prop('disabled', true);


            if (arrayIndex == myArray.length)
                $('#down').css({opacity: '0'})

            //shows down button when greater than zero
            if (arrayIndex > 0)
                $('#up').prop('disabled', false);
        });

        $('#up').click(function () {
            $(myArray[arrayIndex--]).slideDown()

            //makes sure that it never goes below zero for the array
            if (arrayIndex < 0)
                arrayIndex = 0;
            $(myArray[arrayIndex]).slideDown()

            //disables down button when array is at 0 index
            if (arrayIndex == 0)
                $('#up').prop('disabled', true);

            //enables the up button if less than array length
            if (arrayIndex < myArray.length - 1)
                $('#down').prop('disabled', false);
        })

    });

}