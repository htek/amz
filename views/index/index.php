<div class="jumbotron jumbotron-fluid bg-inverse hero--wallpaper">
  <div class="container hero">
    <h1 class="display-4 text-white text-xs-center">A Full-Stack Web Developer</h1> 
		<hr class="my-2">
    <p class="lead text-white text-xs-center">I want to imagine and create robust web applications.</p>
  </div>
</div>
<div class="container">
	<div id="news" class="row">
  </div>
</div>

