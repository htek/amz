<html>
<head>
	<title><?php echo str_replace("/"," ",$_SERVER['REQUEST_URI']); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css?version=1">
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous">
	</script>
	<?php
		if (isset($this->js))
		{
			foreach ($this->js as $js)
			{
				echo '<script src="'.URL.'views/'.$js.'"></script>';
			}
		}
	?>
</head>
<body>
<?php Session::init(); ?>
