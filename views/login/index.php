<div class="container login--form">
<div class="row">
<div class="col-md-6 push-md-3">
<h3 class="text-xs-center">Login</h3></br>
<form class="form" action="login/run" method="post">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email" name="username">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password" name="password">
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Remember me
    </label>
  </div>
  <button type="submit" class="btn btn-success bg-info float-xs-right">Sign in</button>
</form>
</div>
<div class="col-md-3"></div>
</div>
	<div class="row register--link">
		<div class="col-md-6 push-md-3">
			<p class="text-xs-center">
			Or go here to  
			<a href="register">Register</a>
			</p>
		</div>
	</div>
</div>
